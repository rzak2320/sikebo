<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pengajuan extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('logged_in') != TRUE) {
			$notif = array(
				'status' => "gagal",
				'message' => "Silahkan login terlebih dahulu",
			);

			$this->session->set_flashdata($notif);
			redirect('login');
		}

		$this->load->model('M_pengajuan');
		$this->load->model('M_kebutuhan');
		$this->load->model('M_komoditas');
	}

	public function index()
	{
		$data['pengajuan'] = $this->M_pengajuan->list_pengajuan_user('pemesanan')->result();

		$this->load->view('dashboard/sidebar');
		$this->load->view('dashboard/peneliti/pengajuan/index', $data);
		$this->load->view('dashboard/footer');
	}

	public function buat()
	{
		$data = array(
			'komoditas' => $this->M_komoditas->list_komoditas('komoditas')->result(),
			'kebutuhan' => $this->M_kebutuhan->list_kebutuhan('kebutuhan')->result(),
		);

		$this->load->view('dashboard/sidebar');
		$this->load->view('dashboard/peneliti/pengajuan/create', $data);
		$this->load->view('dashboard/footer');
	}

	function get_lokasi()
	{
		$id = $this->input->post('id');
		$data = $this->M_pengajuan->ambil_lokasi($id);

		echo json_encode($data);
	}

	public function tambah()
	{
		$judul = $this->input->post('judul');
		$tanggal = $this->input->post('tanggal');
		$lokasi = $this->input->post('lokasi');
		$komoditas = $this->input->post('komoditas');
		$luas = $this->input->post('luas');
		$check = $this->input->post('kebutuhan[]');
		$jumlah = count($check);
		$pengolahan = $this->input->post('tpengolahan');
		$pemupukan = $this->input->post('tpemupukan');
		$pengairan = $this->input->post('tpengairan');
		$penyiangan= $this->input->post('tpenyiangan');
		$penyemprotan = $this->input->post('tpenyemprotan');
		$panen = $this->input->post('tpanen');

		$pemesanan = array(
			'id_pemesanan' => $this->M_pengajuan->code(),
			'id_user' => $this->session->userdata('id'),
			'id_lokasi' => $lokasi,
			'luas_pakai' => $luas,
			'judul_penelitian' => $judul,
			'tgl_penelitian' => $tanggal,
			'id_komoditas' => $komoditas,
			'status_pemesanan' => "pending",
		);
		for($i=0;$i<$jumlah;$i++){
			$detail[$i] = array(
				'id_pemesanan' => $this->M_pengajuan->code(),
				'id_kebutuhan' => $check[$i],
			);
			// var_dump($detail);
		}
		
		$kegiatan = array(
			'id_pemesanan' => $this->M_pengajuan->code(),
			'pengolahan' => $pengolahan,
			'pemupukan' => $pemupukan,
			'pengairan' => $pengairan,
			'penyiangan' => $penyiangan,
			'penyemprotan' => $penyemprotan,
			'panen' => $panen,
		);

		// var_dump($detail);

		$this->M_pengajuan->pemesanan($pemesanan,'pemesanan');
		$this->M_pengajuan->kebutuhan($detail,'detail_pemesanan');
		$this->M_pengajuan->kegiatan($kegiatan,'detail_kegiatan');
		$notif = array(
			'status' => "berhasil",
			'message' => "Pengajuan berhasil dikirim, pengajuan akan diperiksa terlebih dahulu",
		);
		$this->session->set_flashdata($notif);
		redirect('Peneliti/Pengajuan');
	}
}

/* End of file Pengajuan.php */
/* Location: ./application/controllers/Pengajuan.php */
