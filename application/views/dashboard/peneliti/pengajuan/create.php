<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<?php echo form_open('Peneliti/Pengajuan/tambah') ?>
						<div class="form-group">
							<label class="control-label" for="Judul">Judul Penelitian</label>
							<input type="text" name="judul" class="form-control" id="judul" >
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label class="control_label">Tanggal Penggunaan</label>
										<input type="text" class="form-control pull-right datepicker" name="tanggal" placeholder="yyyy-mm-dd">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label" for="komoditas">Komoditas</label>
										<select name="komoditas" class="form-control select2" style="width: 100%" id="komoditas">
											<option>-- Pilih Komoditas --</option>
											<?php foreach ($komoditas as $k) { ?>
												<option value="<?php echo $k->id_komoditas ?>"><?php echo $k->nama_komoditas ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label class="control-label" for="lokasi">Lokasi</label>
										<select name="lokasi" class="form-control lokasi" style="width: 100%">
											<option>-- Pilih Lokasi --</option>
										</select>
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label class="control-label" for="luas">Luas Lahan</label>
										<input type="text" name="luas" class="form-control" id="luas" placeholder="(meter persegi)" >
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">Kebutuhan</label><br>
										<?php foreach ($kebutuhan as $k) { ?>
											<input type="checkbox" name="kebutuhan[]" class="" value="<?php echo $k->id_kebutuhan ?>"><?php echo $k->nama_kebutuhan ?><br>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label class="control_label">Tanggal Pengolahan Lahan</label>
										<input type="text" class="form-control pull-right datepicker" name="tpengolahan" placeholder="yyyy-mm-dd">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control_label">Tanggal Pemupukan</label>
										<input type="text" class="form-control pull-right datepicker" name="tpemupukan" placeholder="yyyy-mm-dd">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control_label">Tanggal Pengairan</label>
										<input type="text" class="form-control pull-right datepicker" name="tpengairan" placeholder="yyyy-mm-dd">
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label class="control_label">Tanggal Penyiangan</label>
										<input type="text" class="form-control pull-right datepicker" name="tpenyiangan" placeholder="yyyy-mm-dd">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control_label">Tanggal Penyemprotan</label>
										<input type="text" class="form-control pull-right datepicker" name="tpenyemprotan" placeholder="yyyy-mm-dd">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control_label">Tanggal Panen</label>
										<input type="text" class="form-control pull-right datepicker" name="tpanen" placeholder="yyyy-mm-dd">
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	//select kabupaten
	$(document).ready(function() {
		$('#komoditas').change(function() {
			var id = $(this).val();
			$.ajax({
				url: "<?php echo base_url() ?>Peneliti/Pengajuan/get_lokasi",
				method: "POST",
				data: {
					id: id
				},
				dataType: 'json',
				success: function(data) {
					var html = '';
					var i;
					for (i = 0; i < data.length; i++) {
						//html += '<option>'+data[i].nama_kabupaten+'</option>';
						html += '<option value=' + data[i].id_lokasi + '>' + data[i].nama_lokasi + '</option>';
					}
					$('.lokasi').html(html);
				}
			});
		});
	});
</script>