<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_pengajuan extends CI_Model
{

	function list_pengajuan_user($table)
	{
		$this->db->select('pemesanan.*,komoditas.nama_komoditas,user.nama');
		$this->db->join('komoditas','komoditas.id_komoditas=pemesanan.id_komoditas');
		$this->db->join('user','user.id_user=pemesanan.id_user');
		$this->db->where('pemesanan.id_user', $this->session->userdata('id'));
		return $this->db->get($table);
	}

	function ambil_lokasi($id)
	{
		$this->db->select('lokasi.*,komoditas.*,kelompok.*');
		$this->db->join('kelompok', 'kelompok.id_kelompok=lokasi.id_kelompok');
		$this->db->join('komoditas', 'komoditas.id_kelompok=kelompok.id_kelompok');
		$this->db->where('id_komoditas', $id);
		return $this->db->get('lokasi')->result();
	}

	function code(){
        $this->db->select('Right(pemesanan.id_pemesanan,4) as kode ',false);
        $this->db->order_by('id_pemesanan', 'desc');
        $this->db->limit(1);
        $query = $this->db->get('pemesanan');
        if($query->num_rows()<>0){
            $data = $query->row();
            $kode = intval($data->kode)+1;
        }else{
            $kode = 1;

        }
       	$kodemax = str_pad($kode,4,"0",STR_PAD_LEFT);
        $kodejadi  = "PES-".$kodemax;
        return $kodejadi;
    }

	function pemesanan($pemesanan,$table){
		$this->db->insert($table,$pemesanan);
	}

	function kebutuhan($detail,$table){
		$this->db->insert_batch($table,$detail);
	}

	function kegiatan($kegiatan,$table){
		$this->db->insert($table,$kegiatan);
	}
}

/* End of file M_pengajuan.php */
/* Location: ./application/models/M_pengajuan.php */
