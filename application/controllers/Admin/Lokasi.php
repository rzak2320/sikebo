<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Lokasi extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('logged_in') != TRUE) {
			$notif = array(
				'status' => "gagal",
				'message' => "Silahkan login terlebih dahulu",
			);
			$this->session->set_flashdata($notif);
			redirect('login');
		} else {
			if ($this->session->userdata('akses') != "admin") {
				$notif = array(
					'status' => "gagal",
					'message' => "Maaf anda tidak memiliki akses untuk ini !",
				);
				$this->session->set_flashdata($notif);
				redirect('Dashboard');
			}
		}
		$this->load->model('M_lokasi');
		$this->load->model('M_kelompok');
	}

	public function index()
	{
		$data = array(
			'lokasi' => $this->M_lokasi->list_lokasi('lokasi')->result(),
			'kelompok' => $this->M_kelompok->list_kelompok('kelompok')->result(),
		);

		$this->load->view('dashboard/sidebar');
		$this->load->view('dashboard/admin/master/lokasi/index', $data);
		$this->load->view('dashboard/footer');
	}

	public function tambah()
	{
		$kelompok = $this->input->post('kelompok');
		$lokasi = $this->input->post('lokasi');
		$luas = $this->input->post('luas');

		$cek = $this->M_lokasi->cek($lokasi, 'lokasi');

		if ($cek->num_rows() > 0) {
			$notif = array(
				'status' => "gagal",
				'notif' => "Lokasi gagal ditambahkan. " . $lokasi . " sudah ada",
			);

			$this->session->set_flashdata($notif);
			redirect('Admin/Lokasi');
		} else {
			$notif = array(
				'status' => "berhasil",
				'message' => "Lokasi berhasil ditambahkan",
			);
			$data = array(
				'id_kelompok' => $kelompok,
				'nama_lokasi' => $lokasi,
				'luas' => $luas,
			);

			$this->session->set_flashdata($notif);
			$this->M_lokasi->create($data, 'lokasi');
			redirect('Admin/Lokasi');
		}
	}

	public function edit($id)
	{
		$where = array('id_lokasi' => $id);
		$data = array(
			'lokasi' => $this->M_lokasi->ambil($where, 'lokasi')->row_array(),
			'kelompok' => $this->M_kelompok->list_kelompok('kelompok')->result(),
		);

		$this->load->view('dashboard/sidebar');
		$this->load->view('dashboard/admin/master/lokasi/edit', $data);
		$this->load->view('dashboard/footer');
	}

	public function update()
	{
		$id = $this->input->post('id');
		$kelompok = $this->input->post('kelompok');
		$lokasi = $this->input->post('lokasi');
		$luas = $this->input->post('luas');

		$where = array('id_lokasi' => $id);
		$data = array(
			'id_kelompok' => $kelompok,
			'nama_lokasi' => $lokasi,
			'luas' => $luas,
		);
		$notif = array(
			'status' => "berhasil",
			'message' => "Lokasi berhasil diperbarui",
		);

		$this->session->set_flashdata($notif);
		$this->M_lokasi->replace($where, $data, 'lokasi');
		redirect('Admin/Lokasi');
	}

	public function hapus($id)
	{
		$notif = array(
			'status' => "berhasil",
			'message' => "Lokasi berhasil dihapus",
		);
		$where = array('id_lokasi' => $id);

		$this->session->set_flashdata($notif);
		$this->M_lokasi->trash($where, 'lokasi');
		redirect('Admin/Lokasi');
	}
}

/* End of file Lokasi.php */
/* Location: ./application/controllers/Lokasi.php */
