<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kegiatan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('logged_in') != TRUE){
			$notif = array(
				'status' => "gagal",
				'message' => "Silahkan login terlebih dahulu",
			);
			$this->session->set_flashdata($notif);
			redirect('login');
		}else{
			if($this->session->userdata('akses') != "admin"){
				$notif = array(
					'status' => "gagal",
					'message' => "Maaf anda tidak memiliki akses untuk ini !",
				);
				$this->session->set_flashdata($notif);
				redirect('Dashboard');
			}
		}
		$this->load->model('M_kegiatan');
	}

	public function index()
	{
		$data['kegiatan'] = $this->M_kegiatan->list_kegiatan('kegiatan')->result();
		$this->load->view('dashboard/sidebar');
		$this->load->view('dashboard/admin/master/kegiatan/index',$data);
		$this->load->view('dashboard/footer');
	}

	public function tambah(){
		$kegiatan = $this->input->post('kegiatan');

		$data = array('nama_kegiatan' => $kegiatan);
		$cek = $this->M_kegiatan->cek($kegiatan,'kegiatan');

		if($cek->num_rows() > 0){
			$notif = array(
				'status' => "gagal",
				'message' => "Data gagal ditambahkan, kegiatan ".$kegiatan." sudah ada, silahkan cek kembali",
			);

			$this->session->set_flashdata($notif);
			redirect('Admin/kegiatan');
		}else{
			$notif = array(
				'status' => "berhasil",
				'message' => "Data berhasil disimpan",
			);

			$this->M_kegiatan->create($data,'kegiatan');
			$this->session->set_flashdata($notif);
			redirect('Admin/Kegiatan');
		}
	}

	public function edit(){
		$id = $this->input->post('id');

		$result = $this->M_kegiatan->get($id,'kegiatan');

		echo json_encode($result);
	}

	public function update(){
		$kegiatan = $this->input->post('kegiatan');

		$where = array('id_kegiatan' => $this->input->post('id'));
		$data = array('nama_kegiatan' => $kegiatan);

		$cek = $this->M_kegiatan->cek($kegiatan,'kegiatan');

		if($cek->num_rows() > 0){
			$notif = array(
				'status' => "gagal",
				'message' => "Data gagal diperbarui, kegiatan ".$kegiatan." sudah ada, silahkan cek kembali",
			);

			$this->session->set_flashdata($notif);
			redirect('Admin/Kegiatan'); 
		}else{
			$notif = array(
				'status' => "berhasil",
				'message' => "Data berhasil diperbarui",
			);

			$this->session->set_flashdata($notif);
			$this->M_kegiatan->replace($where,$data,'kegiatan');
			redirect('Admin/Kegiatan');
		}
	}

	public function hapus($id){
		$notif = array(
			'status' => "berhasil",
			'message' => "Data berhasil dihapus",
		);
		$where = array('id_kegiatan' => $id);

		$this->session->set_flashdata($notif);
		$this->M_kegiatan->trash($where,'kegiatan');
		redirect('Admin/Kegiatan');
	}

}

/* End of file Kegiatan.php */
/* Location: ./application/controllers/Kegiatan.php */