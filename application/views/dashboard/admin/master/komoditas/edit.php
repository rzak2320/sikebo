<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<?php if ($this->session->flashdata('status') == "gagal") { ?>
					<div class="alert alert-danger"><?php echo $this->session->flashdata('message') ?></div>
				<?php } ?>
				<?php if ($this->session->flashdata('status') == "berhasil") { ?>
					<div class="alert alert-success"><?php echo $this->session->flashdata('message') ?></div>
				<?php } ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="box">
					<div class="box-body">
						<?php echo form_open('Admin/komoditas/update') ?>
						<div class="form-group">
							<label class="control-label" for="kelompok">Kelompok</label>
							<select name="kelompok" class="select" style="width: 100%" id="kelompok">
								<option>-- Pilih Kelompok--</option>
								<?php foreach ($kelompok as $k) { ?>
									<option value="<?php echo $k->id_kelompok ?>" <?php if ($komoditas['id_kelompok'] == $k->id_kelompok) {
																						echo "selected";
																					} ?>><?php echo $k->nama_kelompok ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group">
							<label class="control-label" for="komoditas">Nama komoditas</label>
							<input type="text" name="komoditas" class="form-control" value="<?php echo $komoditas['nama_komoditas'] ?>" id="komoditas" required>
						</div>
						<div class="form-group">
							<input type="hidden" name="id" class="form-control" value="<?php echo $komoditas['id_komoditas'] ?>" required readonly>
							<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>