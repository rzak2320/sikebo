<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kelompok extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('logged_in') != TRUE){
			$notif = array(
				'status' => "gagal",
				'message' => "Silahkan login terlebih dahulu",
			);
			$this->session->set_flashdata($notif);
			redirect('login');
		}else{
			if($this->session->userdata('akses') != "admin"){
				$notif = array(
					'status' => "gagal",
					'message' => "Maaf anda tidak memiliki akses untuk ini !",
				);
				$this->session->set_flashdata($notif);
				redirect('Dashboard');
			}
		}
		$this->load->model('M_kelompok');
	}

	public function index()
	{
		$data['kelompok'] = $this->M_kelompok->list_kelompok('kelompok')->result();
		$this->load->view('dashboard/sidebar');
		$this->load->view('dashboard/admin/master/komoditas/kelompok',$data);
		$this->load->view('dashboard/footer');
	}

	public function tambah(){
		$kelompok = $this->input->post('kelompok');

		$data = array('nama_kelompok' => $kelompok);
		$cek = $this->M_kelompok->cek($kelompok,'kelompok');

		if($cek->num_rows() > 0){
			$notif = array(
				'status' => "gagal",
				'message' => "Data gagal ditambahkan, kelompok ".$kelompok." sudah ada, silahkan cek kembali",
			);

			$this->session->set_flashdata($notif);
			redirect('Admin/Kelompok');
		}else{
			$notif = array(
				'status' => "berhasil",
				'message' => "Data berhasil disimpan",
			);

			$this->M_kelompok->create($data,'kelompok');
			$this->session->set_flashdata($notif);
			redirect('Admin/Kelompok');
		}
	}

	public function edit(){
		$id = $this->input->post('id');

		$result = $this->M_kelompok->get($id,'kelompok');

		echo json_encode($result);
	}

	public function update(){
		$kelompok = $this->input->post('kelompok');

		$where = array('id_kelompok' => $this->input->post('id'));
		$data = array('nama_kelompok' => $kelompok);

		$cek = $this->M_kelompok->cek($kelompok,'kelompok');

		if($cek->num_rows() > 0){
			$notif = array(
				'status' => "gagal",
				'message' => "Data gagal diperbarui, kelompok ".$kelompok." sudah ada, silahkan cek kembali",
			);

			$this->session->set_flashdata($notif);
			redirect('Admin/Kelompok'); 
		}else{
			$notif = array(
				'status' => "berhasil",
				'message' => "Data berhasil diperbarui",
			);

			$this->session->set_flashdata($notif);
			$this->M_kelompok->replace($where,$data,'kelompok');
			redirect('Admin/Kelompok');
		}
	}

	public function hapus($id){
		$notif = array(
			'status' => "berhasil",
			'message' => "Data berhasil dihapus",
		);
		$where = array('id_kelompok' => $id);

		$this->session->set_flashdata($notif);
		$this->M_kelompok->trash($where,'kelompok');
		redirect('Admin/Kelompok');
	}

}

/* End of file Kelompok.php */
/* Location: ./application/controllers/Kelompok.php */