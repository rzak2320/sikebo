<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_lokasi extends CI_Model
{

	function list_lokasi($table)
	{
		$this->db->select('kelompok.nama_kelompok,lokasi.*');
		$this->db->join('kelompok', 'kelompok.id_kelompok=lokasi.id_kelompok');
		return $this->db->get($table);
	}

	function cek($lokasi, $table)
	{
		$this->db->where('nama_lokasi', $lokasi);
		return $this->db->get($table);
	}

	function create($data, $table)
	{
		$this->db->insert($table, $data);
	}

	function ambil($where, $table)
	{
		$this->db->where($where);
		return $this->db->get($table);
	}

	function replace($where, $data, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	function trash($where, $table)
	{
		$this->db->where($where);
		$this->db->delete($table);
	}
}

/* End of file M_lokasi.php */
/* Location: ./application/models/M_lokasi.php */
