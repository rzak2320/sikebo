<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_kegiatan extends CI_Model {

	function list_kegiatan($table){
		return $this->db->get($table);
	}

	function cek($kegiatan,$table){
		$this->db->where('nama_kegiatan',$kegiatan);
		return $this->db->get($table);
	}

	function create($data,$table){
		$this->db->insert($table,$data);
	}

	function get($id,$table){
		$this->db->where('id_kegiatan',$id);
		return $this->db->get($table)->row_array();
	}

	function replace($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}

	function trash($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}

}

/* End of file M_kegiatan.php */
/* Location: ./application/models/M_kegiatan.php */