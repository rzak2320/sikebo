<div class="content-wrapper">
	<section class="content-header">
		<h1>Daftar Pengajuan</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header">
						<a href="<?php echo site_url('Peneliti/Pengajuan/buat') ?>" class="btn btn-success"><i class="fa fa-plus"></i> Ajukan</a>
					</div>
					<div class="box-body">
						<table class="table table-bordered table-striped" id="table">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="text-center">Judul Penelitian</th>
									<th class="text-center">Komoditas</th>
									<th class="text-center">Status</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php $no = 1;
								foreach ($pengajuan as $p) { ?>
									<tr>
										<td class="text-center"><?php echo $no++ ?></td>
										<td><?php echo $p->judul_penelitian ?></td>
										<td><?php echo $p->nama_komoditas ?></td>
										<td class="text-center"><?php echo $p->status_pemesanan ?></td>
										<td class="text-center">
											<a href="<?php echo site_url('Peneliti/Pengajuan/edit/' . $p->id_pemesanan) ?>" class="btn btn-info btn-xs"><i class="fa fa-edit"></i></a>
											<a href="#" data-url="<?php echo site_url('Admin/Pengajuan/hapus/' . $p->id_pemesanan) ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>